BUILD_PATH := packages/apps/ViPER4AndroidFX

PRODUCT_SOONG_NAMESPACES += $(BUILD_PATH)

PRODUCT_PACKAGES += \
    libv4a_fx \
    ViPER4AndroidFX
